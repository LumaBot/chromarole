# Import Discord API
import discord
from discord.ext import commands

# Import misc. libraries
import math
import colorsys
import os
from dotenv import load_dotenv

# Loads the .env file that resides on the same level as the script.
load_dotenv()

# Grab the API token from the .env file.
DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")

description = "A bot for organizing roles by color."

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix='?', description=description, intents=intents)

# Function definition for color sorting
# Copied from https://www.alanzucconi.com/2015/09/30/colour-sorting/
def step (r,g,b, repetitions=1):
    lum = math.sqrt( .241 * r + .691 * g + .068 * b )
    h, s, v = colorsys.rgb_to_hsv(r,g,b)
    h2 = int(h * repetitions)
    lum2 = int(lum * repetitions)
    v2 = int(v * repetitions)
    if h2 % 2 == 1:
        v2 = repetitions - v2
        lum = repetitions - lum
    return (h2, lum, v2)

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    print('------')

@bot.command()
async def role_msg(ctx, channel: int, *args: str):
    chnl = bot.get_channel(channel)
    if(chnl == None):
        chnl = ctx
    guild = ctx.guild
    if guild is None:
        print('Error: Guild not found')
        return

    # Get all roles in the server
    roles = guild.roles
    role_names = []
    for i in range(len(roles)):
        role_names.append(roles[i].name)

    # Get all role objects listed in arguments and their color sorting index
    partial_role_list = []
    for i in range(len(args)):
        if args[i] in role_names:
            role_to_add = roles[role_names.index(args[i])]
            r, g, b = role_to_add.color.to_rgb()
            partial_role_list.append((role_to_add, step(r, g, b, 8)))

    partial_role_list.sort(key=lambda role: role[1])
    msg = ''
    for i in range(len(partial_role_list)):
        msg += f'<@&{partial_role_list[i][0].id}>\n'
    await chnl.send(msg)
    
# EXECUTES THE BOT WITH THE SPECIFIED TOKEN.
bot.run(DISCORD_TOKEN)
